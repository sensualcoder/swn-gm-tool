#include "DiceRollModel.hpp"

namespace SwnGmTool
{
    std::string to_string(const DiceRollModel& model)
    {
        std::string dice_str = std::to_string(model.DiceNum) + "d" + std::to_string(model.DiceType);

        if(model.Modifier > 0)
        {
            dice_str += "+" + std::to_string(model.Modifier);
        }

        return dice_str;
    }
}
