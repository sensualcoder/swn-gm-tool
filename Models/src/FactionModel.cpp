#include "FactionModel.hpp"

namespace SwnGmTool
{
    bool operator==(const FactionModel& a, const FactionModel& b)
    {
        return a.Name == b.Name;
    }

    bool operator<(const FactionModel& a, const FactionModel& b)
    {
        return a.Name < b.Name;
    }
}
