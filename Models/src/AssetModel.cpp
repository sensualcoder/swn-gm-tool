#include "AssetModel.hpp"

namespace SwnGmTool
{
    std::string to_string(const AssetModel& a)
    {
        return (a.Name + " " + a.RatingType + " " + a.AssetType + " " + to_string(a.Attack) + " " + to_string(a.Counter) );
    }

    std::ostream& operator<<(std::ostream& os, const AssetModel& a)
    {
        os << to_string(a);
        return os;
    }

    bool operator==(const AssetModel& a, const AssetModel& b)
    {
        return a.Name == b.Name;
    }
}
