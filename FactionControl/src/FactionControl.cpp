#include "FactionControl.hpp"

#include <algorithm>
#include <cmath>
#include <map>

namespace SwnGmTool
{
    struct RatingXpHp
    {
        uint8_t XP;
        uint8_t HP;
    };

    static const std::map<int, RatingXpHp> RatingXpHpTable
    {
        { 1, { 0, 1 } },
        { 2, { 2, 2 } },
        { 3, { 4, 4 } },
        { 4, { 6, 6 } },
        { 5, { 9, 9 } },
        { 6, { 12, 12 } },
        { 7, { 16, 16 } },
        { 8, { 20, 20 } }
    };

    int CalcHpFromAttributes(const FactionCreateModel& faction)
    {
        return RatingXpHpTable.find(faction.Force)->second.HP
            + RatingXpHpTable.find(faction.Cunning)->second.HP
            + RatingXpHpTable.find(faction.Wealth)->second.HP
            + 4;
    }

    int CalcHpFromAttributes(const FactionModel& faction)
    {
        return RatingXpHpTable.find(faction.Force)->second.HP
            + RatingXpHpTable.find(faction.Cunning)->second.HP
            + RatingXpHpTable.find(faction.Wealth)->second.HP
            + 4;
    }

    int CalcIncomeFromAttributes(const FactionCreateModel& faction)
    {
        return std::ceil(faction.Wealth / 2.0)
            + std::floor( (faction.Force + faction.Cunning) / 4.0);
    }

    int CalcIncomeFromAttributes(const FactionModel& faction)
    {
        return std::ceil(faction.Wealth / 2.0)
            + std::floor( (faction.Force + faction.Cunning) / 4.0);
    }

    // Public methods

    int FactionControl::GetFactionCount() const
    {
        return faction_asset_map_.size();
    }

    std::vector<FactionModel> FactionControl::GetFactionList() const
    {
        std::vector<FactionModel> faction_list;

        if(faction_asset_map_.size() > 0)
        {
            faction_list.reserve(faction_asset_map_.size() );

            for(auto& it : faction_asset_map_)
            {
                faction_list.push_back(it.first);
            }
        }

        return faction_list;
    }

    const FactionModel& FactionControl::GetFactionDetails(int index) const
    {
        auto item = faction_asset_map_.begin();

        std::advance(item, index);

        return item->first;
    }

    void FactionControl::AddFaction(const FactionModel& faction)
    {
        auto fa_pair = std::make_pair(faction, std::list<AssetModel>() );

        faction_asset_map_.push_back(fa_pair);
    }

    void FactionControl::AddFaction(const FactionCreateModel& faction)
    {
        FactionModel model
        {
            .Name = faction.Name,
            .Description = faction.Description,
            .Force = faction.Force,
            .Cunning = faction.Cunning,
            .Wealth = faction.Wealth
        };

        model.MaxHP = model.CurrentHP = CalcHpFromAttributes(faction);

        model.Income = CalcIncomeFromAttributes(faction);

        model.FacCreds = model.Exp = 0;

        model.Tags = faction.Tags;

        this->AddFaction(model);
    }

    void FactionControl::RemoveFaction(int index)
    {
        auto item = faction_asset_map_.begin();
        std::advance(item, index);

        faction_asset_map_.erase(item);
    }

    void FactionControl::ClearMap()
    {
        faction_asset_map_.clear();
    }

    const std::list<AssetModel>& FactionControl::GetAssetList(int index) const
    {
        auto item = faction_asset_map_.begin();
        std::advance(item, index);

        return item->second;
    }

    void FactionControl::AddAsset(int index, const AssetModel& asset)
    {
        auto item = faction_asset_map_.begin();
        std::advance(item, index);

        item->second.push_back(asset);
    }

    void FactionControl::RemoveAsset(int index, int assetIndex)
    {
        auto item = faction_asset_map_.begin();
        std::advance(item, index);

        auto asset = item->second.begin();
        std::advance(asset, assetIndex);

        item->second.erase(asset);
    }

    void FactionControl::RemoveAllAssetsOfType(int index, const AssetModel& asset)
    {
        auto item = faction_asset_map_.begin();
        std::advance(item, index);

        item->second.remove(asset);
    }

    void FactionControl::ClearAssets(int index)
    {
        auto item = faction_asset_map_.begin();
        std::advance(item, index);

        item->second.clear();
    }
}
