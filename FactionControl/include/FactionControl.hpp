#ifndef FACTION_CONTROL_HPP
#define FACTION_CONTROL_HPP

#include <cstdint>
#include <list>
#include <string>
#include <vector>

#include <cereal/cereal.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/utility.hpp>
#include <cereal/types/vector.hpp>

#include "AssetModel.hpp"
#include "FactionModel.hpp"
#include "FactionCreateModel.hpp"

namespace SwnGmTool
{
    class FactionControl
    {
        public:
            FactionControl() = default;
            ~FactionControl() = default;

            int GetFactionCount() const;
            std::vector<FactionModel> GetFactionList() const;
            const FactionModel& GetFactionDetails(int index) const;
            void AddFaction(const FactionModel& faction);
            void AddFaction(const FactionCreateModel& faction);
            void RemoveFaction(int faction);
            void ClearMap();

            const std::list<AssetModel>& GetAssetList(int faction) const;
            void AddAsset(int faction, const AssetModel& asset);
            void RemoveAsset(int faction, int asset);
            void RemoveAllAssetsOfType(int faction, const AssetModel& asset);
            void ClearAssets(int faction);

            template <class Archive>
            void serialize(Archive& archive);

        private:
            std::vector<std::pair<FactionModel, std::list<AssetModel> > > faction_asset_map_;
    };

    template <class Archive>
    void FactionControl::serialize(Archive& archive)
    {
        archive(faction_asset_map_);
    }
}

#endif
