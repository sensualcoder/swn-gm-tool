#include "Driver.hpp"

int main()
{
    Driver::Driver main{};

    main.Run();

    return 0;
}
