#ifndef SWN_GM_TOOL_API_HPP
#define SWN_GM_TOOL_API_HPP

#include <memory>
#include <string>

#include <cereal/cereal.hpp>

#include "ConfigModel.hpp"
#include "FactionControl.hpp"
#include "FactionCreateModel.hpp"

namespace SwnGmTool
{
    class SwnGmToolAPI
    {
        public:
            SwnGmToolAPI(const ConfigModel& config) noexcept;
            SwnGmToolAPI(SwnGmToolAPI&& source) noexcept = default;
            ~SwnGmToolAPI() noexcept = default;

            SwnGmToolAPI& operator=(SwnGmToolAPI&& source) noexcept = default;

            const ConfigModel& GetConfig() const;

            int GetFactionCount() const;
            const std::vector<FactionModel> GetFactionList() const;
            const FactionModel& GetFactionDetails(int faction) const;
            void AddFaction(const FactionModel& faction);
            void AddFaction(const FactionCreateModel& faction);
            void RemoveFaction(int faction);
            void RemoveFaction(const FactionModel& faction);
            void ClearFactions();

            int GetAssetCount(int faction) const;
            const std::list<AssetModel>& GetAssetList(int faction) const;
            void AddAsset(int faction, const AssetModel& asset);
            void RemoveAsset(int faction, int asset);
            void ClearAssets(int faction);

            template <class Archive>
            void serialize(Archive& archive)
            {
                archive(cereal::make_nvp("Faction Control", *this->SGTFactionControl) );
            }

        private:
            ConfigModel SGTConfig;

            std::unique_ptr<FactionControl> SGTFactionControl;
    };
}

#endif
