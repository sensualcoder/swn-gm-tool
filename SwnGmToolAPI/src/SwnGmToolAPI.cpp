#include "SwnGmToolAPI.hpp"

namespace SwnGmTool
{
    SwnGmToolAPI::SwnGmToolAPI(const ConfigModel& config) noexcept
        : SGTConfig(config), 
            SGTFactionControl(new FactionControl() )
    {
    }

    const ConfigModel& SwnGmToolAPI::GetConfig() const
    { 
        return this->SGTConfig; 
    }

    int SwnGmToolAPI::GetFactionCount() const
    {
        return this->SGTFactionControl->GetFactionCount();
    }

    const std::vector<FactionModel> SwnGmToolAPI::GetFactionList() const
    { 
        return this->SGTFactionControl->GetFactionList();
    }

    const FactionModel& SwnGmToolAPI::GetFactionDetails(int index) const
    {
        return this->SGTFactionControl->GetFactionDetails(index);
    }

    void SwnGmToolAPI::AddFaction(const FactionModel& model) 
    { 
        this->SGTFactionControl->AddFaction(model); 
    }

    void SwnGmToolAPI::AddFaction(const FactionCreateModel& model) 
    { 
        this->SGTFactionControl->AddFaction(model); 
    }

    void SwnGmToolAPI::ClearFactions() 
    { 
        this->SGTFactionControl->ClearMap(); 
    }

    const std::list<AssetModel>& SwnGmToolAPI::GetAssetList(int factionIndex) const
    {
        return this->SGTFactionControl->GetAssetList(factionIndex);
    }

    void SwnGmToolAPI::AddAsset(int factionIndex, const AssetModel& asset)
    {
        this->SGTFactionControl->AddAsset(factionIndex, asset);
    }

    void SwnGmToolAPI::RemoveAsset(int factionIndex, int assetIndex)
    {
        this->SGTFactionControl->RemoveAsset(factionIndex, assetIndex);
    }
    
    void SwnGmToolAPI::ClearAssets(int factionIndex)
    {
        this->SGTFactionControl->ClearAssets(factionIndex);
    }
}