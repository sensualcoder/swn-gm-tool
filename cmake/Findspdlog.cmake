include(LibFindMacros)

libfind_pkg_check_modules(spdlog_PKGCONF spdlog)

find_path(spdlog_INCLUDE_DIR
    NAMES spdlog.h
    PATHS ${spdlog_PKGCONF_INCLUDE_DIRS}
)

libfind_process(spdlog)
