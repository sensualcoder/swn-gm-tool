include(LibFindMacros)

libfind_pkg_check_modules(cereal_PKGCONF cereal)

find_path(cereal_INCLUDE_DIR
    NAMES cereal.hpp
    PATHS ${cereal_PKGCONF_INCLUDE_DIRS}
)

libfind_process(cereal)
