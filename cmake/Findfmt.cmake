include(LibFindMacros)

libfind_pkg_check_modules(fmt_PKGCONF fmt)

find_path(fmt_INCLUDE_DIR
    NAMES core.h
    PATHS ${fmt_PKGCONF_INCLUDE_DIRS}
)

find_library(fmt_LIBRARY
    NAMES fmt
    PATHS ${fmt_PKGCONF_LIBRARY_DIRS}
)

libfind_process(fmt)
