#include "SwnHex.hpp"

namespace SwnGmTool
{
    SwnHex createSwnHex(const HexGrid::Hex& hex)
    {
        return SwnHex { hex, "", {} };
    }
}
