#ifndef SWN_HEX_HPP
#define SWN_HEX_HPP

#include <string>
#include <vector>

#include "Hex.hpp"
#include "Planet.hpp"

namespace SwnGmTool
{
    struct SwnHex
    {
        HexGrid::Hex HexRef;
        std::string SystemName;
        std::vector<Planet> Planets;
    };

    SwnHex createSwnHex(const HexGrid::Hex& hex);
}

#endif
