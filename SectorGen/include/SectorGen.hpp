#ifndef SECTOR_GEN_HPP
#define SECTOR_GEN_HPP

#include <list>
#include <memory>

#include "HexMap.hpp"
#include "Planet.hpp"
#include "SwnHex.hpp"

namespace SwnGmTool
{
    class SectorGen
    {
        public:
            SectorGen();
            SectorGen(int width, int height);

            void CreateFTVRMap(int width, int height);

            void GenerateSector();
            void GenerateSector(int star_minimum);
            void ClearSector();

            int GetMapSize();
            int GetSystemListSize();

            std::list<SwnHex> GetSystemList() const;
            std::list<Planet> GetPlanetList() const;

        private:
            int MapWidth;
            int MapHeight;

            HexGrid::HexMap SectorMap;
            std::list<SwnHex> SystemList;
    };
}

#endif
