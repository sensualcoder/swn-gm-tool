#ifndef SECTOR_CONSTANTS_HPP
#define SECTOR_CONSTANTS_HPP

namespace SwnGmTool
{
    static const int STD_MAP_WIDTH = 8;
    static const int STD_MAP_HEIGHT = 10;
    static const int MIN_STAR_MOD = 20;
}

#endif // SECTOR_CONSTANTS_HPP
