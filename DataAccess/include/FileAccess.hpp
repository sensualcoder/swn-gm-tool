#ifndef FILE_ACCESS_HPP
#define FILE_ACCESS_HPP

#include "DataAccess.hpp"

#include <iostream>
#include <string>

#include <cereal/cereal.hpp>
#include <cereal/archives/json.hpp>

namespace SwnGmTool
{
    template<typename T>
    class FileAccess final : public DataAccess<T>
    {
        public:
            FileAccess() = default;

            void Save(std::ostream& out, T& toSave, std::string name) override;
            void Load(std::istream& in, T& toLoad) override;
    };

    template<typename T>
    void FileAccess<T>::Save(std::ostream& out, T& toSave, std::string name)
    {
        cereal::JSONOutputArchive archive(out);

        try
        {
            archive(cereal::make_nvp(name, toSave) );
        }
        catch(cereal::RapidJSONException& ex)
        {
            throw(ex);
        }
    }

    template<typename T>
    void FileAccess<T>::Load(std::istream& in, T& toLoad)
    {
        cereal::JSONInputArchive archive(in);

        try
        {
            archive(toLoad);
        }
        catch(cereal::RapidJSONException& ex)
        {
            throw(ex);
        }
    }
}

#endif
